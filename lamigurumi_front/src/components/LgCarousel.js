import React from 'react';

import carouselImg from '../assets/carousel.png'; // Tell Webpack this JS file uses this image


class LgCarousel extends React.Component {
    render() {
        return (
            <img alt="carousel" src={carouselImg} width="100%" height="100%"/>
        )
    }
}

export default LgCarousel;
