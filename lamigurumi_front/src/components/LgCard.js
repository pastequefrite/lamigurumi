import React from "react";
import {
    Card,
    CardImg,
    CardFooter,
} from "shards-react";


class LgCard extends React.Component {
    render() {
        return (
            <Card style={{maxWidth: 200}}>
                <CardImg src={this.props.image}/>
                <CardFooter>{this.props.title}</CardFooter>
            </Card>
        );
    }
}

export default LgCard;
