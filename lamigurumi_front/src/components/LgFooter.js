import React from "react";
import {StyleSheet, css} from 'aphrodite';
import {faYoutube, faFacebook, faInstagram} from "@fortawesome/free-brands-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


class LgFooter extends React.Component {
    render() {
        return (
            <div className={css(styles.container)}>
                <div className={css(styles.socialContainer)}>
                    <a className={css(styles.socialItem)} rel="noopener" href="https://www.youtube.com"><FontAwesomeIcon icon={faYoutube} size={"2x"}/></a>
                    <a className={css(styles.socialItem)} rel="noopener" href="https://www.facebook.com"><FontAwesomeIcon icon={faFacebook} size={"2x"}/></a>
                    <a className={css(styles.socialItem)} rel="noopener" href="https://www.instagram.com"><FontAwesomeIcon icon={faInstagram} size={"2x"}/></a>
                </div>
                <div className={css(styles.copyright)}>
                    <p><a href="http://localhost:3000">Lamigurumi.fr</a> {new Date().getFullYear()} | PastequeFriteProduction</p>
                </div>
            </div>
        );
    }
}

export default LgFooter;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        padding: 15,
    },
    socialContainer: {
        alignSelf: 'center',
        justifySelf: 'center',
    },
    copyright: {
        alignSelf: 'center',
        justifySelf: 'center',
        height: 20,
    },
    socialItem: {
        padding: 15,
        color: 'rgba(0,0,0,.5)',
        ':hover': {
            color: 'rgba(0,0,0,.9)',
        }
    }
});
