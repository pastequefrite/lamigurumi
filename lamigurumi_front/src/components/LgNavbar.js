import React from "react";

import {NavLink as RouterDomNavLink} from 'react-router-dom'
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    NavLink,
    FormInput,
    Collapse
} from "shards-react";

export default class LgNavbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: false,
            collapseOpen: false
        };
    };

    toggleNavbar = () => {
        this.setState({
            ...this.state,
            ...{
                collapseOpen: !this.state.collapseOpen
            }
        });
    };

    render() {
        return (
            <Navbar expand="md" type="light">
                <NavbarBrand>
                    L'ami Gurumi
                </NavbarBrand>
                <NavbarToggler onClick={this.toggleNavbar}/>

                <Collapse open={this.state.collapseOpen} navbar>
                    <Nav navbar>
                        <NavItem>
                            <NavLink tag={RouterDomNavLink} to="/" exact>
                                Accueil
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterDomNavLink} to="/products">
                                Nos produits
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterDomNavLink} to="/about">
                                A propos
                            </NavLink>
                        </NavItem>
                    </Nav>

                    <Nav navbar className="ml-auto">
                        <InputGroup size="sm" seamless>
                            <InputGroupAddon type="prepend">
                                <InputGroupText>
                                    <FontAwesomeIcon icon={faSearch}/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <FormInput className="border-0" placeholder="Rechercher..."/>
                        </InputGroup>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}
