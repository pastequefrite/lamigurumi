import React from 'react';
import {StyleSheet, css} from 'aphrodite';

import {Container, Row, Col} from 'shards-react';

import LgCard from '../components/LgCard';
import LgCarousel from '../components/LgCarousel';


class HomePage extends React.Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col className="d-flex justify-content-center">
                        <LgCarousel/>
                    </Col>
                </Row>
                <Row>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                    <Col className={css(styles.item)}>
                        <LgCard image="https://place-hold.it/200x200" title="Le titre" />
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default HomePage;


const styles = StyleSheet.create({
    item : {
        paddingBottom: 15,
        paddingTop: 15,
        display: 'flex',
        justifyContent: 'center',
    }
});
